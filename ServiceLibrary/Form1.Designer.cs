﻿using System.Windows.Forms;

namespace ServiceLibrary
{
    partial class iIUT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.Titre = new System.Windows.Forms.Label();
            this.btn_add_dep = new System.Windows.Forms.Button();
            this.Liste_dep = new System.Windows.Forms.ComboBox();
            this.btn_access_dep = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Titre
            // 
            this.Titre.AutoSize = true;
            this.Titre.Location = new System.Drawing.Point(249, 78);
            this.Titre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Titre.Name = "Titre";
            this.Titre.Size = new System.Drawing.Size(83, 13);
            this.Titre.TabIndex = 0;
            this.Titre.Text = "Gestion de l\'IUT";
            // 
            // btn_add_dep
            // 
            this.btn_add_dep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btn_add_dep.Location = new System.Drawing.Point(52, 278);
            this.btn_add_dep.Name = "btn_add_dep";
            this.btn_add_dep.Size = new System.Drawing.Size(136, 73);
            this.btn_add_dep.TabIndex = 1;
            this.btn_add_dep.Text = "Ajouter un département";
            this.btn_add_dep.UseVisualStyleBackColor = true;
            // 
            // Liste_dep
            // 
            this.Liste_dep.FormattingEnabled = true;
            this.Liste_dep.Items.AddRange(new object[] {
            "Informatique",
            "GEA",
            "TC",
            "GB",
            "Informatique"});
            this.Liste_dep.Location = new System.Drawing.Point(227, 176);
            this.Liste_dep.Name = "Liste_dep";
            this.Liste_dep.Size = new System.Drawing.Size(121, 21);
            this.Liste_dep.TabIndex = 2;
            this.Liste_dep.Text = "Départements";
            // 
            // btn_access_dep
            // 
            this.btn_access_dep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btn_access_dep.Location = new System.Drawing.Point(362, 278);
            this.btn_access_dep.Name = "btn_access_dep";
            this.btn_access_dep.Size = new System.Drawing.Size(136, 73);
            this.btn_access_dep.TabIndex = 3;
            this.btn_access_dep.Text = "Accéder au département";
            this.btn_access_dep.UseVisualStyleBackColor = true;
            this.btn_access_dep.Click += new System.EventHandler(this.btn_access_dep_Click);
            // 
            // iIUT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.btn_access_dep);
            this.Controls.Add(this.Liste_dep);
            this.Controls.Add(this.btn_add_dep);
            this.Controls.Add(this.Titre);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "iIUT";
            this.Text = "IUT";
            this.ResumeLayout(false);
            this.PerformLayout();

            }

        #endregion

        private System.Windows.Forms.Label Titre;
        private System.Windows.Forms.Button btn_add_dep;
        private ComboBox Liste_dep; 
        private Button btn_access_dep;

        
    }
}